import { Component } from '@angular/core';
import { Platform, MenuController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-first',
  templateUrl: './first.page.html',
  styleUrls: ['./first.page.scss']
})
export class FirstPage {
  private backButtonSub: Subscription;

  constructor(
    private platform: Platform,
    private menuCtrl: MenuController,
    private navCtrl: NavController
  ) {}

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton.subscribeWithPriority(
      10000,
      () => this.onBack()
    );
  }

  ionViewWillLeave() {
    this.backButtonSub.unsubscribe();
  }

  private async onBack() {
    const openMenu = await this.menuCtrl.getOpen();
    if (openMenu) {
      await openMenu.close();
    } else {
      await this.navCtrl.pop();
    }
  }
}
