import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss']
})
export class ModalPage {
  constructor(private alertCtrl: AlertController) {}

  public async openAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      buttons: ['Ok'],
      backdropDismiss: false
    });
    return await alert.present();
  }
}
