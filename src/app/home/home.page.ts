import { ModalPage } from './../modal/modal.page';
import { Component } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(
    private modalCtrl: ModalController,
    private navCtrl: NavController
  ) {}

  public async openModal() {
    const modal = await this.modalCtrl.create({
      component: ModalPage
    });
    return await modal.present();
  }

  public async openPage() {
    await this.navCtrl.navigateForward('/first');
  }
}
